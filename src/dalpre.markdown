---
title: Dalpre
categories:
  - Major Topic
events:
  - event: point
    when: 1333 TSC
    culture: tar
    title: Genetic Magic is established as a branch of magical  research
  - event: point
    when: 1335 TSC
    culture: tar
    title: The first dalpre is born
  - event: point
    when: 1401 TSC
    culture: tar
    title: Kormar passes the first law banning genetic magic on  dalpre
  - event: point
    when: 1391 TSC
    culture: tar
    title: The Inheritance Gene is invented
  - event: point
    when: 1395 TSC
    culture: tar
    title: Over 80% of all living dalpre have the Inheritance Gene
  - event: point
    when: 1403 TSC
    culture: tar
    title: Over 95% of all living dalpre have the Inheritance Gene
---

Dalpre are a race of humans that were magically grafted to various mammals to create a slave caste.

# Genetic Magic

In 1333 TSC, a number of research mages independently stumble on a a branch of magic related to genetics and enchanting offspring through reproduction wards and spells. While initially used to enhance offspring, it quickly paved the way into creating a slave race of humans grafted with mammals, the dalpre.

In the years that followed, research in the dalpre became a lucrative career for mages. This resulted in the dalpre spreading across much of [Tarsan](), [Gepaul](), and [Kormar]().

Genetic magic was banned in 1345 TSC. By then, the dalpre were considered no longer human and further research continued at a breakneck pace as the dalpre were subjected to further manipulations and abuse. Magic was used to emphasize specific personality traits, enhancing vulnerabilities, breeding in submissive traits, supressing magical talents, and reducing intelligence. Specific lines of genetics were prized and dalpre were considered no more than mere cattle or pets.

It wasn't until 1401 TSC that manipulating dalpre through magic was specifically banned. In 1405, laws against non-magical forms of breeding for selective traits were established throughout the known world. In hindsight, it was considered a placating gesture since the dalpre were already disadvantaged socially, economically, and magically.

# Inheritance Gene

While dalpre were created as slaves, they still bred with non-dalpre (nadalpre), occasionally producing dalpre offspring. This established a period of time (1335 to 1405 TSC) when social and legal systems struggled with the concept of inheritance with the slave race.

This changed when magical research created the Inheritance Gene, a genetic spell that prevents the offspring of a dalpre from ever being anything but a dalpre.

The Inheritance Gene was also the final straw when it came to genetic research on dalpre. As the gene spread through new breeds and was imprinted on existing dalpre, so did the laws that finally banned genetic magic on all living creatures including dalpre.

# Mandate of Ear and Tail

After decades of activism, [Council of Countries]() made a mandate that all dalpre had the full rights and privileges of other humans. Announced in 1837 TSC, the [Mandate of Ear and Tail]() was met with derision and naked hatred. Many short-lived wars and civil disturbances flared across most of the known world.

The Mandate was effectively ignored. Many non-dalpre made little or no effort to tell dalpre they were free. There were almost as many cases of dalpre actively prevented from being told about the Mandate.
