---
title: United Hidanork Tribes
aliases:
  - UHT
categories:
  - Country
summary: >
  A loose federation of nomadic tribes occupying the steppes of the northern continent.
---

Situated in the northern part of the continent, the United Hidanork Tribes represent a loose federation of nomadic tribes occupying the steppes.
