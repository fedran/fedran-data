---
title: Piróma
aliases:
  - Róma
categories:
  - Person
  - Shimusògo Clan Born
events:
  - event: birth
    when: 1485/2/19 MTR 1::1
    references: [Mapábyo, Rutejìmo, Kitòpi]
    relatives:
      - character: Mapábyo
        relationship: Mother
      - character: Rutejìmo
        relationship: Father
---

Piróma (Róma for short) was the youngest child of [Rutejìmo]() and [Mapábyo]().
