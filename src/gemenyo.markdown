---
title: Gemènyo
aliases:
  - Shimusogo Gemènyo
categories:
  - Person
  - Shimusògo Clan Member
  - Clan Member
summary: "A courier of the [Shimusògo]() and best friends with [Hyonèku]() and [Desòchu](). He smoked a pipe almost constantly."
events:
  - event: birth
    when: 1435/3/34 MTR 19::37
    secondary:
      - Nyachobíma (Mother)
      - Datichìo (Father)
    referenced:
      - Myodáto (Sister)
      - Pichyóra (Sister)
  - event: point
    title: "[Gemènyo]() became romantic with [Kiríshi]()"
    when: 1453/1/7 MTR
    secondary:
      - Kiríshi
  - event: point
    title: "[Gemènyo]() broke up with [Kiríshi]()"
    when: 1455/1/40 MTR
    secondary:
      - Kiríshi
  - event: point
    title: "[Gemènyo]() became romantic with [Faríhyo]()"
    when: 1456/4/19 MTR
    secondary:
      - Faríhyo
  - event: death
    when: 1482/4/30 MTR 16::30
notes: >
  Eldest would become a horse rider of [Ryayusúki]()

  Other sister would join the [Aematsūka]() clan.
---

Gemènyo (Mènyo) was a courier of the [Shimusògo]() clan. He was best friends with [Hyonèku]() and [Desòchu](). Throughout most of his adulthood, he smoked a pipe almost constantly ever since he got it as a gift from a grateful stranger.

# Early Life

Born the third child to [Nyachobíma]() and [Datichìo](), Gemènyo was their first born son. Both of his parents were native [Shimusògo]() as were his two older sisters, [Myodáto]() and [Pichyóra](). All four adored him. In keeping in line with the Shimusògo way, they also pushed him to keep up with them. He had a firm and loving family and he loved them back completely.

Like most children in the valley, as he got older, he was left to entertain himself when not doing chores. As one of the eldest of his generation, he spent much of his time alone until Kiríshi and the others grew up enough to be left alone. His brief isolation is also when he got his first chemical addiction, chewing the slightly narcotic wood of the [matakila]() bushes he found along the furthest end of the valley.

# A Calm Voice

His passage into adulthood was relatively sudden when he had a urge to walk out of the valley. He did and started running. The elders that were planning on watching over him had to scramble to catch up but there ended up being little concern. Despite normally having the rites attract the attention of the various clan spirits, it was obvious that only [Shimusògo]() would lay claim to Gemènyo and he was equally suited to the clan spirit. Four days later, he came back with the powers of the clan spirit and chatting with the other clan members as a peer.

His real talents become obvious when he was allowed to speak during the clan meetings. After a few years of stumbling, he started to speak up in the clan meetings. His compassion and gregarious nature won him the right to speak. His sensible ideas and long-term planning earned him respect of his elders.

Gemènyo always had a deft skill at guiding through suggestions and advice. Occasionally, he did the sardonic comments or a wry statement. As the desert society prided leading others through a gentle touch, this added to the respect the clan had for him.

# Raging Alone

By the events of [Raging Alone](), Gemènyo and [Desòchu](), [Kiríshi]() and [Hyonèku]() were all close friends.

> He turned toward Gemènyo's voice. His friend stood on a lower trail, the light behind him masking his face in a shadow. Desòchu's memories filled in the rest: his friend's broad nose, the easy smile on his lips, and the wide eyebrows over green eyes. From the corner, he could see a twig of matakila bobbing slowly as Gemènyo chewed on it.
>
> --- [Raging Alone 6](/raging-alone/chapter-06/)

```plugin
type: spoiler
source: Raging Alone
```

Gemènyo had already gone through his rite of passage but he continued to hang around the teenagers because of his relationship with Kiríshi.

> Sadness flickered across Desòchu's thoughts. Kiríshi was right on the edge of her passage into adulthood. The elders were waiting for something and none of the teenagers knew what. Until they, they were willing to let Gemènyo pretend he wasn't an adult until she caught up. But if she wasn't holding him back, he was going to be drawn into the world of responsibilities and obligations. He was going to lose one of his friends soon, at least until he was considered an adult himself.

Gemènyo had been with Kiríshi since he was seventeen and most of the clan planned on them becoming a couple. However, they drifted away during the events of *Raging Alone* when Kiríshi started taking a more dedicated interest in children. Gemènyo realized he liked children but he wasn't willing to commit to the same degree as his girlfriend. When she started spending more time with Hyonèku, they were initially secretive but Gemènyo quickly figured it out. To their surprise, he gave his blessings. From his point of view, he valued their friendship more than jealousy.

# His Second Addiction

On one of Gemènyo's courier runs by himself, he was given a gift of a stone pipe by a stranger he had met during his rite. At that point, he had never smoked nor had anyone in the [Shimusogo Valley]() done so. He honored the gift and learned how to use it, quickly becoming addicted to smoking weed. Ever since, he had carried the pipe with him and only put it away when he was doing long-distance running.

# The New Girl

It wasn't until some years later that Gemènyo would find a love. [Faríhyo]() came to the valley with both of her parents, both of the [Tateshyúso]() clan. She was still a six years his junior and hadn't been through to her rights of of passage. Gemènyo was nothing more than a big brother, introducing her to the surrounding clans, but soon after she was declared an adult, their relationship became more romantic.

# Sand and Blood

> Gemènyo's dark-skinned form welled out of the darkness. In the lantern light, the clan courier was a blot of shadows except for bright teeth and the whites around his eyes. Smoke rose from a pipe he held with three fingers. In his other hand, he carried a half-full bottle of what appeared to be fermented milk, the strongest alcoholic drink in the valley. He was slightly taller than [Rutejìmo](), with curly black hair. Unlike many of the other adult men in the valley, he kept no beard along his brown chin. He wore a pair of trousers but no shirt, his usual outfit for wandering along the valley. The trousers were a deep red, one of the two colors of Shimusògo.

```plugin
type: spoiler
source: Sand and Blood
```

Gemènyo's role of trying to subtly guide the youth of the valley was well established by the events of [Sand and Blood](). He interrupted [Tejíko]() when she was beating Rutejìmo for trying to steal ashes.

> Rutejìmo's grandmother let out an exasperated sigh. "This is none of your business, Gemènyo."
>
> "I just wanted to make sure the screams of a little child were for a good reason."
>
> "He tried to steal [Byodenóre's](/byodenore/) ashes."
>
> "Oh, did he succeed or fail?"
>
> "Failed, of course."
>
> Gemènyo waved his pipe in the air. "Then I agree, a beating is appropriate here. Please, go right ahead, Great Shimusogo Tejíko."

He was also one of the clan elders who went with the teenagers during their rite of passage and watched from a distance.

# The Rutejìmo Problem

In the year after [Rutejìmo]() returned to the valley as an adult, it was obvious that he was struggling to take the next steps of becoming an adult. He skipped out on every celebration, insisted on wearing Mikáryo's claw necklace, and generally kept to himself.

When the others in the clan suggested their usual method of solving most problems---send Rutejìmo out in the desert to figure it out---Gemènyo resisted. He didn't think that isolation in the desert would be the right answer. Instead, he suggested the clan allow Rutejìmo to remain in the clan but find his own path. His suggestion was not taken well, but in the few months that followed, he managed to sway the decision and the clan decided to let quietly Rutejìmo remain but without the restrictions normally enforced by the clan's social mores.

It was accepted that Gemènyo would have taken leadership over the clan once [Tejíko]() died. Normally leadership was decided by the oldest surviving member, there was an uncomfortable realization among the clan adults that [Somiryòki]() was the next in line and his dementia would be a hindrance to the clan.

# Children

It took many years before Faríhyo and Gemènyo decided to have children. Neither wanted to have one before they were ready, but once they were, they quickly had their daughter, [Nigímo](). During the events of [Sand and Ash](), they were working toward a second.

# Sand and Ash

```plugin
type: spoiler
source: Sand and Ash
```

By the time *Sand and Ash* started, the clan's tolerance of Rutejìmo's differences had run its course. Both [Desòchu]() and [Tejíko]() had lost their patience and Gemènyo's words weren't enough. Still feeling that abandoning Rutejìmo in the desert wouldn't be effective, Gemènyo came up with an alternative to handle two problems in the clan: Rutejìmo's own path and [Mapábyo's](/mapabyo/) obvious affection for him.

The original plan was to get Rutejìmo to go alone with Mapábyo along her courier route. What no one expected was Mikáryo. After Rutejìmo ran off with her without telling the clan, there was a tense few months of calling in favors trying to find him.

Gemènyo would have tried to stop Desòchu from ostracizing Rutejìmo. However, Gemènyo and [Kiríshi]() independently took Mapábyo to the side and suggested she go back for Rutejìmo.

When Mapábyo started staying away from [Shimusogo Valley](), it was obvious that she had fallen in love with Rutejìmo. To bring her back and keep her from drifting from the clan, a group went to [Wamifuko City]() to retrieve the two.

No one expected how Desòchu's attitude would change with Rutejìmo's presence in the valley. Like Rutejìmo's own path, the warrior's mood soured over time. Things hit a boiling point when a drunk Desòchu confronted Rutejìmo in the valley. Hearing the rush of wind and the raised voice, Gemènyo and [Kiríshi]() ran over to ensure it didn't turn violent.

The tension between the two brothers continued until one day when Rutejìmo suddenly broke his enforced silence from becoming a [banyosiōu]() to warn them that their daughter was about to fall off the watch cliff at the valley entrance. They both rushed up to catch her, but Nigímo was on the guard wires. Faríhyo overextended herself to catch her before their daughter fell to her death.

Seeing that he was about to lose his wife and daughter, Gemènyo used his speed powers to throw them to safety even though the momentum would cause him to fall off the cliff. His last words to Faríhyo was "I'll wait for you" before he plummeted.
