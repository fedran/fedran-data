---
title: Lily
aliases:
  - Lily kia Kasin
categories:
  - Person
  - Second-Hand Dresses Character
  - Second-Hand Dresses Primary Character
sources:
  - Second-Hand Dresses
summary: >
  A disgraced spinster with the power to change the color of fabric and yarns.
characterPov: "0102"
---

Lily was born to
A disgraced spinster with the power to change the color of fabric and yarns.
