---
title: Countries
alias:
  - Country
---

The term country refers to a political state or nation or its territory. It is often referred to as the land of a individual's birth, residence, or citizenship.

```plugin
type: insert-category
key: categories
value: Country
```
