---
title: Ryozapòti
categories:
  - Kyōti Clan
  - Tachìra Clan
---

The Ryozapòti is a small [Kyōti]() clan located on the western part of the Mifuno Desert. They are allies with the [Shimusògo]() and are a herd spirit.
