---
title: Lorban
categories:
  - Language
summary: >
  The most common language spoken in the northwestern
  part of the continent.
---

Lorban is the most common language spoke across [Tarsan](), [Gepaul](), and [Kormar]().

# Notable Elements

There is no "c" in Lorban because there is typically one way to pronounce a word. Typically this means "s", "x", or "k" is used instead.

# Naming Conventions

Lorban names typically begin and end with consonants. It is considered rude to use a name that has vowels in the beginning and end, mainly because there is a required pause to separate the name from the surrounding words.
