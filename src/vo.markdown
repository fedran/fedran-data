---
title: Vo
categories:
  - Country
aliases:
  - Isle of Vo
summary: >
  A large island off the western coast of the continent where telepaths live.
---

A large island off the western coast of the continent where telepaths live.
