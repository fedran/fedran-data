---
title: Pyābi
categories:
  - Kyōti Topic
  - Currency
summary: >
  The primary currency of [Kyōti](/kyoti/) with a common abbreviation of "p".
---

A primary unit of currency of [Kyōti](/kyoti/), the pyābi is a coin imprinted from aluminum bronze. The denominations are: quarter, half, one, three, nine, twenty, sixty, and two hundred.

While there are local mints from various clans of the [Mifuno Desert]() that control the designs on both sides of the coins along with adding various attempts to avoid counterfeits, there are certain common features:

- All denominations have the same size starting with one inch and increasing a third inch across.
- The edges of the coins are "reeded" with a denomination-specific design.
- The weight of the coins are precisely measured.
- The designs change annually.
