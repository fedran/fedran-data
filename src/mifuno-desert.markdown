---
title: Mifuno Desert
---

The Mifuno Desert is the name of the desert that stretches across all of [Kyōti](). It is the physical manifestation of the desert spirit, [Mifúno]().
