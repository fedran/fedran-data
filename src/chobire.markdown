---
title: Chobìre
categories:
  - Kyōti Clan
---

Chobìre is one of the three great spirits of [Kyōti](). As the spirit of the moon, he battles constantly with [Tachìra](), the sun, for the affection of [Mifúno](), the desert. This conflict has drawn in the rest of the spirits of Kyōti who gain power from either Chobìre or Tachira.

Like Tachìra's dependence on sunlight, Chobìre's power are only functional while the moon is above the horizon.

# Members

There are no known people who directly use the Chobìre name.

# Clans

```plugin
type: insert-category
key: categories
value: Chobìre Clan
```
