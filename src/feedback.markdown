---
title: Feedback
categories:
  - Magic Topic
---

Feedback is the sometimes violent interaction of disharmonious [resonance](). It is one of primary limiters of magic and the reason advances in magic stalled out until advances in [fire cores](/fire-core/) triggered the end of the [Crystal Age]() and the beginning of the [Mechanical Age]().

# Between Talents

The most common response of feedback between two people with [talents](/talent/) typically include:

- Itchy joints
- Headaches
- Blurred vision
- Dizziness

In this cases, talents have relatively low power compared to mages but because their magic is fairly stable, the results remain the same.

# Mages

[Mages](/mage/) have it far worse than talents. As their power increases, as does their susceptibility to feedback along with the amount of feedback they produce in others. When the feedback between two talents may only cause a headache, one of those same talents could cause nosebleeds or migraines in a mage of relatively high power.

Related, two mages can cause exponentially more severe feedback responses with each other. What was just a blinding headache with a talent could cause blackouts, collapses, or even apparent concussions in a matter of hours.

At severe levels, two powerful mages in close proximity could kill each other in a matter of minutes. It is rarely a pleasant death.

## Isolation

Because of feedback, mages of advanced power have a tendency to live far away from civilization. That way, their vulnerabilities and the impact they have on others is minimized.
