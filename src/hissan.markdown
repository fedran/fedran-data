---
title: Hissan
categories:
  - Language
summary: >
  The spoken and written language of the [United Hidanork Tribes]().
task:
  - Create a full repository for Hissan.
---

Hissan is the language written and spoken by the [United Hidanork Tribes]().
