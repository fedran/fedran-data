---
title: Sun-Over-Sands
categories:
  - Country
events:
  - event: point
    when: 33.5.1953 TSC
    culture: tar
    title: Sun-Over-Sands established as a country
---

It would be 271 years before the country fell apart once again to civil war.
