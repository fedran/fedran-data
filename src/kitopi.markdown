---
title: Kitòpi
aliases:
  - Tòpi
categories:
  - Person
  - Shimusògo Clan Born
events:
  - event: birth
    when: 1483/6/19 MTR 3::41
    references: [Mapábyo, Rutejìmo]
    relatives:
      - character: Mapábyo
        relationship: Mother
      - character: Rutejìmo
        relationship: Father
---

Kitòpi (Tòpi for short) was the eldest son of [Rutejìmo]() and [Mapábyo]().
