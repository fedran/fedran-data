---
title: Tarsan Standard Calendar
categories:
  - Calendar
  - Tarsan Topic
---

The Tarsan Standard Calendar (TSC) is the most accepted calendar system used throughout the world.

1. Kastir (KM): 49 days
2. Dobmahin (DM): 49 days
4. Rinfirm (RM): 51 days
5. Punmahik (PM): 50 days
6. Joktip (JM): 51 days
7. Lamaster (LM): 50 days
8. Wilmahil (WM): 52 days
9. Xahos mahar (XM): 16 days
  - One additional day on years evenly divisible by three
  - Two days lost on years evenly divisible by eighty-one

# Date Formats

However, to indicate a date, the short form of date (detri) of det is used to indicate the following is a date. The format is just the numbers without the "li" terminated by maha (month), dei (day), naha (year). Numbers are always zero-based. A month name can be used instead of the number.

- det # maha # dei # naha

For example, for Punmahik 11, 1832, any of the following are correct:

- det vo maha papa dei pabiciro naha
- det Punmahik maha papa dei pabiciro naha
- det Punmahik papa dei pabiciro naha (can skip named months)
- det py.my. maha papa dei pabiciro naha (py.my. is "PM")

In cultures that have numbers, a shorter form uses periods to separate the names. This format is slowly beginning to be popular in Gepaul but not in Tarsan. Zero padding is optional, but typically not used.

- 4.11.1832

# Historical Changes

The initial calendar started with seven periods, masti, one for each of the seven families. Each masti is fifty-two days.

1. Kasin mastir
2. Disrobin mastin
3. Rinfir mastim
4. Pun mastik
5. Joknig mastip
6. Lamaster mastit
7. Wilim mastil

Fortunately, Tarsan is obsessed with "order", so this means we can get away with only the acronym of the names for the months.

- 364 = KM 52, DM 52, RM 52, PM 52, JM 52, LM 52, WM 52

## Formalizing the weeks

To break up the longer months, the concept of a jeftu (week, short form is jef) is introduced. Each jeftu is seven days long, starting with the first day of each month. The first day is nodei (zero day). Each month has a remainder of days, a torjef (short week).

## It doesn't take long

It was only a few years after the calendar was established that the fight over the calendar itself became apparent. Since the creators of the calendar were Gof da Joknig de Wilim and Fahin dea Lamaster, they were given additional days for their family's months. The reason it took so long was because there was actually a small war over this change with Lamaster taking offense because they were only going to add one to their month even though Fahin's mother and father were from the same family.

- 368 = KM 52, DM 52, RM 52, PM 52, JM 53, LM 54, WM 53

## A bit of infighting

The Disrobin family starts a fight with the Rinfir. After a few decades of fighting, it ends when the Disrobin almost goes bankrupt and loses a great deal of face. Unable to recoup the full costs of the fight, the Rinfir steals a day.

- 368 = KM 52, DM 51, RM 53, PM 52, JM 53, LM 54, WM 53

## Formalizing the bells

Up to this point, most people only referred to the parts of the days into vague organization.

1. Early morning
2. Late morning
3. Midday
4. Early evening
5. Late evening
6. All of the night

The first five became the foundation of the bells, a tradition of ringing a towns bells ten times throughout the day and night, each one spread evenly throughout the time. Like many numbers in Tarsan, the first is the zero-bell all the way up to the ninth bell.

## The past is never over

One of the greatest hobbies in Tarsan is digging up dirt for someone's ancestors. This usually involves a lot of back-stabbing, gossip, and blackmail to figure out, but once it comes out in public, the results are usually swift.

The worst crime of Tarsan is spoiling a bloodline. When something like this happens, typically every descendant's accomplishments are taken away and their misdeeds magnified.

As it came to be, Fahin dea Lamaster's grandfather on his father's side was revealed to not be a male and also not the direct bloodline of the Lamaster family. What happened was Fahin's father's parents were actually a lesbian couple with one of them masquerading as a male for the last forty years of her life. This was possible thanks to magic, but in a patriarchal society (linage is equal but only males controlled families at the time), this was a scandal. It also negated, in society's eye, Fahin's contribution to the calendar and the two days were stripped from their month. They stripped another day because of the deception.

One would think that a fairly accurate calendar (368 days) would not be made more inaccurate, but the politics were stronger than research at the time. It would be three centuries before they would get back to the 368 day calendar.

- 365 = KM 52, DM 51, RM 53, PM 52, JM 53, LM 51, WM 53

## Further breaking down the day

The length of time with the bells became a little too long for many tastes. Each bell was split into two, the divison called a cacra (hours, cra). This lead to a twenty-hour day (which is what I meant when I say hours in my novels because it's "close enough.")

Likewise, the hours were broken further down into mentu (minutes, ntu). There are sixty-four minutes in an hour. This number was decided by the woman (a talent with perfect time-keeping) to honor the seven great families plus her own. This broken up the hours into eight parts and then each one of those broken into eight more which resulted in the sixty-four minutes.

## Derobin and Disobin split

The Disrobin family broke apart a century after the last calendar change and kicked off a civil war that lasted nearly forty years. The reasons were wide and varied, but the split was so great that the Derobin family actually split completely from Tarsan and formed a new country of Gepaul.

One of the consequences of this was a diminution of three of the month names from the formal masti to mahi.

- Disrobin mastin became Dobmahin.
- Pun mastik became Punmahik.
- Wilim mastil became Wilmahil.

## Clawing back the days

Three hundred years after the three days were taken from the calendar, there was another battle (yeah, Tarsan fought a lot with each other and Gepaul). Three lesser families (Pinnir, Xahos, and Tibirim) who aided Rinfir and Kasin during a border dispute where deemed worth of a great reward. As the nature of the great families, these "great rewards" were typically useless posturing or empty gifts tied with strings. And what would be better than to give these allies three brand new days named after them? But, they weren't the original great family, so they got a single day each (yes, Tarsan is petty). With Pinnir having the same letter as Pun, the lesser families used two letter abbreviations.

1. Kasin mastir (KM)
2. Dobmahin (DM)
3. Pinnir mahap (PiM)
4. Rinfir mastim (RM)
5. Punmahik (PM)
6. Xahos mahar (XaM)
7. Joknig mastip (JM)
8. Lamaster mastit (LM)
9. Wilmahil (WM)
10. Tibirim mahan (TiM)

Sticking with the same block. The shorter months are considered to be all torjef or short weeks.

- 368 = KM 52, DM 51, Pim 1, RM 53, PM 52, XaM 1, JM 53, LM 51, WM 53, TiM 1

## Further diminutions

Time always corrupts languages, usually causing the names to be shortened. Most of these were the more complicated ones. There was a lot of effort by some of the other families not have their names shortened (think Xerox's attempt to save their name).

1. Kastir (KM)
2. Pinhap (PiM)
3. Joktip (JM)
4. Lamaster (LM)

The actual days didn't change.

- 368 = KM 52, DM 51, Pim 1, RM 53, PM 52, XaM 1, JM 53, LM 51, WM 53, TiM 1

## Bit of backroom politics

Tarsan remains a patriarchy even to the events of the novels and [Journals of Fedran](). Much of the marriage involves dowries. There are also trade agreements or exchanges of power. A number of these happened over the next century, but we can include them in a single step.

- Lamaster lost a day to Tibirim mahan.
- Kasim mastir lost a day to Dobmahin.
- Wilmahil lost a day to Rinfir mastim.

This results in the following block.

- 368 = KM 51, DM 52, Pim 1, RM 54, PM 52, XaM 1, JM 53, LM 50, WM 52, TiM 2

## Finally, getting some partial days

Over a hundred fifty years after the desert clans figured out fall days, Tarsan finally accepted that the year doesn't fit neatly in a given days. Actually, it is well accepted that the idea was stolen from the desert cultures (after the Fimùchi Calculations) but Tarsan had to do things their own way. Instead of two days losing one every four and one every eight, they did something else.

- Moved the short months combined into one at the end of the year.
- The Xahos mahar is retained but the "i" is dropped.
- There is an additional day once every three years (effectively adding 0.3 to the days).

And the results.

- 368.333 = KM 51, DM 52, RM 54, PM 52, JM 53, LM 50, WM 52, XM 4.333

## The lesser families gain power

Over the next century or so, there were more battles and marriages, shuffling around the days. As the lesser families gained power in the country, their days were moved over to Xahos mahar.

In addition, Rinfir mastim was reduced to Rinfirm.

1. Kastir (KM) lost 2 days to XM
2. Dobmahin (DM) lost 3 days to XM
4. Rinfirm (RM) lost 3 days to XM
5. Punmahik (PM) lost 2 days to XM
6. Joktip (JM) lost 2 days to XM
7. Lamaster (LM)
8. Wilmahil (WM)
9. Xahos mahar (XM) gained 12 days

This evened out the months a bit.

- 368.333 = KM 49, DM 49, RM 51, PM 50, JM 51, LM 50, WM 52, XM 16.333

## The Tabrin Withdrawal

The Tabrin family, a very small one with only a single estate on top of a mountain dedicated their life to observation of the sun and moon. After a number of decades, they proposed a single to the calendar to bring it in line. Normally a change like this would be laughed out of the families, but the Tabrin managed to pull of an amazing number of favors, deals, and ties that it was accepted almost unanimously.

- Remove two days from Xahos mahar every 81 years. (-0.025 days).

The Tabrin family didn't explain how they came up with this change. They refused to answer until the Tarsan great families decided to press the issue. They sent a contingent of a hundred warriors (a rekihobehe) to arrest the entire family, only to find that every single member of the Tabrin family dead. Later investigations suggested that the Tabrin were killed before the proposal was ever made.

Despite pressure to ignore the Tabrin changes, it was accepted. In fact, the final decision by the great families of Tarsan made a ruling that they would allow changes to the calendar by unanimous decision by all the members. This final calendar is what would become known as the Tarsan Standard Calendar.

- 368.308 = KM 49, DM 49, RM 51, PM 50, JM 51, LM 50, WM 52, XM 16.308

# Holidays and significant dates

Tarsan has a number of celebration days (detsalci).

- One for each "birth" of each of the great families.
- The day Tarsan declared victory over Gepaul.

In addition, each family has their own holidays and special dates (lazdetsalci) which is celebrated both in the family and their cities (identified with "Ku family").

Tarsan society, as a whole, is obsessed with correctness. This includes date. When a celebration day is established, it is tracked against the "correct" day even across changes. This means that any date before the final change travels across the calendar (a "traveling date" or litrudetri).
