---
title: Byodenóre
aliases:
  - Shimusogo Byodenóre
categories:
  - Person
  - Shimusògo Clan Member
  - Clan Member
  - Sand and Blood Character
  - Sand and Blood Tertiary Character
summary: A former courier of the [Shimusògo]() clan who died delivering his final message.
events:
  - event: birth
    when: 1378/2/15 MTR 4::32
    culture: kyo
---

Byodenóre is a courier of the [Shimusògo]() clan. He ran until the last day of his life, dying from an attack but not before delivering one final message. He was honored in the [Shimusogo Shrine]().

# Sand and Blood

In the first chapter of [Sand and Blood](), [Rutejìmo]() attempts to steal Byodenóre's ashes to prove his skills. He is caught by [Hyonèku]() and ordered to report to Byodenóre's and Rutejìmo's grandmother, [Tejíko]().

```plugin
type: spoiler
source: Sand and Blood
```

When Tejíko found out, she beat Rutejìmo until [Gemènyo]() interrupted her.

> Rutejìmo's grandmother let out an exasperated sigh. "This is none of your business, Gemènyo."
>
> "I just wanted to make sure the screams of a little child were for a good reason."
>
> "He tried to steal Byodenóre's ashes."
>
> "Oh, did he succeed or fail?"
>
> "Failed, of course."
>
> Gemènyo waved his pipe in the air. "Then I agree, a beating is appropriate here. Please, go right ahead, Great Shimusogo Tejíko."

# Relationships

* [Tejíko](): Daughter
