---
title: Turfuno
categories:
  - Country
events:
  - event: point
    when: "23.5.1844 TSC"
    culture: tar
    title: "[Lankerni]() establishes the territorial boundaries of [Turfuno]()"
---

Turfuno was a small nation created by the combined efforts of [Tarsan](), [Gepaul](), [Kormar](), and [Lankerni]() to give the [dalpre]() a country to call their own.

# Terrain

The land granted by [Lankerni]() was a rocky desert with poor access to clear water, no farmland, and few natural resources. It had only a single ocean port in [Solistoin Bay]().

# Flag

![Turfuno Flag](turfuno-flag-01.svg){.right-50}

The official Turfuno flag was created soon after the country was created and the first of the dalpre began to arrive. Despaired by the lack of resources and poor prospects, the impromptu leaders of the country used the flag as a beacon of hope that they would eventually create a green land where they would thrive.

The four stars in the corners represent the four other countries who worked together to give the dalpre a home.

Finally, the two intertwined tails represent the dalpre working together to create a new home.

# Kyōti

Despite bordering against the [Mifuno Desert](), no dalpre was allowed to enter the desert. This caused a massive military buildup from both sides in a stalemate. The stalemate lasted until the [Treaty of Golden Steps]() in 1856 TSC that dalpre permitted to enter the desert without risking death.

# Sun-Over-Sands

In 1953 TSC, civil wars that tore Turfuno apart were finally resolved with the [Treaty of Suns](). Reunified as a single country, the disparate kingdoms turned away from the name given to them by Tarsan and renamed themselves [Sun-Over-Sands]().
