---
title: Calendars
categories:
  - Major Topic
---

Because Fedran encompasses a wide variety of cultures and histories, there are a number of overlapping and complementary calendar and date systems that show up in stories and in this site.

```plugin
type: insert-category
template: list
key: categories
value: Calendar
```
