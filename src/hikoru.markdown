---
title: Hikòru
aliases:
  - Shimusogo Hikòru
categories:
  - Person
  - Shimusògo Clan Member
  - Clan Member
events:
  - event: birth
    when: 1422/2/22 MTR 19::25
    secondary:
      - Tejíko
  - event: point
    title: "[Hikòru]() declared banyosiōu by [Tejíko]()"
    when: 1456/1/7 MTR
  - event: death
    when: 1456/1/11 MTR 10::32
---

Hikòru is a courier of the [Shimusògo]() clan who was later struck from public record when he was declared [dead to the clan](/banyosiou/) for excessive drinking, endangering the rest of the clan, and the desire for suicide.

# Relationships

* [Tejíko](): Mother
* [Chyojímo](): Wife
* [Desòchu](): Son
* [Rutejìmo](): Son
