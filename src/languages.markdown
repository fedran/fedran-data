---
title: Languages
categories:
  - Major Topic
---

Across history and the continent, there were a wide variety of languages that are spoken, written, and have been forgotten. Not to mention, there are languages that have been oppressed and ones that are being actively suppressed.

```plugin
type: insert-category
template: list
key: categories
value: Language
```
