---
title: Nigímo
events:
  - event: birth
    when: 1480/2/41 MTR 2::51
    secondary:
      - Faríhyo (Mother)
      - Gemènyo (Father)
---
