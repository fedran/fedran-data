---
title: Chyojímo
aliases:
  - Shimusogo Chyojímo
categories:
  - Person
  - Shimusògo Clan Member
  - Clan Member
events:
  - event: birth
    when: 1419/7/9 MTR 4::51
  - event: death
    when: 1454/7/42 MTR 13::25
    references: [Rutejìmo, Desòchu, Hikòru]
---

Chyojímo is a courier of the [Shimusògo]() clan.

# Life

Chyojimo always wanted four children in her life, two girls and two boys. When her [first son](/shimusogo-desochu/) was born, she was estatic. But, her later attempts ended in a series of miscarriages that left her weak and unable to run with the rest of her clan. After the third miscarriage, she retired from being a courier and remained in [Shimusogo Valley]() with the elders.

Her second son, [Rutejìmo](), was a happy surprise but her pregnancy was plagued with difficulties. When he was born, she lost a lot of blood and many of the clan didn't think she would make it but she was determined. Only a few weeks after Rutejìmo was born, she tripped on a path and fell. The injuries from her fall was too much and she died soon after being rescued.
