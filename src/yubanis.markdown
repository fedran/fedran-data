---
title: Yubanis
events:
  - event: birth
    when: 9.4.1829 TSC 12.47
    secondary:
      - Penilil (Mother)
      - Goisay (Father)
    referenced:
      - Tubocak (Brother)
---

Yubanis (9.4.1829 TSC) spent his entire life inside the boundaries of the [United Hidanork Tribes](). Despite having a relatively calm childhood, he manifested a powerful regeneration ability that helped him survive the early years of the [Mechanical War]().

A [moot child](), Yubanis was born to [Penilil]() in 1829 TSC. He was her second child and, ultimately, her youngest son. Penilil never knew the name of his biological father, but [Goisay]()---her husband---treated Yubanis as his child as per the normal customer of the tribes.

Compared to his brother, [Tubocak](), Yubanis wasn't remarkable until he manifested a powerful regeneration ability. His power was capable of healing cuts nearly instantly, repairing broken bones in a matter of seconds, and even saved him repeatedly from injuries that would have killed others.

> "Yuba managed to crawl out after her and he dragged her away until the rest of us caught up with them. When I got there, she was already unconscious from the poison but he was knelt over her, bleeding from hundreds of wounds and crying."
>
> It was only a few dozen bites but Yubanis didn't correct his brother's exaggerations.
>
> "I asked the tiag for help with finding something for the venom. It took me so long and I couldn't find enough. I was sure I was going to lost one of them that day."
>
> Around them, the natural lights flared brighter for a moment and a fox yipped loudly.
>
> "But when I got back, he was planting a warning post with our dad near the snake pit without a single scratch. Which was good because I could only find enough herbs for one of them." Tubocak sighed. "I'm glad the tiag told me it was okay, she knew what my brother was capable of."
>
> --- [In Search of a Cat 4](/in-search-of-a-cat/chapter-04/)

Despite his powers, he remained in his older brother's shadow due to Tubocak's having one of the most treasured abilities among the tribes: the ability to sense and communicate with [taigs]().

# The Grand Moot

Yubanis got his [bloodline tattoos]() before his first grand moot in 1849 TSC. His tattoos includes a stylized version of chains on his right side for his mother's blood lines and ram horns for his biological father.

```plugin
type: spoiler
source: Looking for the Wrong Thing
```

In the early hours of the moot, Yubanis tried to distinguish himself in the sparring circles, but his brawling skills were severely lacking. Only his regeneration kept him going long after most others would have given up.

One of the [ring mothers](), [Ami]() realized that his talents were better suited in a different arena and brought him over to the drinking games. Fronting a small purse to join into an advanced game, she got him involved in a game with her husband.

Thanks to his regeneration ability, Yubanis found that he was barely affected by even the strongest of drinks and handily won the game. His prize was a stack of rings.

Ami decided to reward him with a prize of her own: the rest of the night with her company. Despite the moot being a moment of procreation, she would not end up having his child.

# The Poisoned Tiag

```plugin
type: spoiler
source: In Search of a Cat
```

Months later and well after the moot, Yubanis and his family continued along their [ancestral route](). Along the way, they get embroiled in a search for another family's cat. He met up with [Unil]() and experienced his first death when he tried to save her from being injured.
