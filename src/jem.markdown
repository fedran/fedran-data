---
title: Jem
aliases: [Jems]
summary: >
  The primary currency of [Tarsan](/tarsan/) with a standard abbreviation of "j".
categories:
  - Currency
  - Tarsan Topic
events:
  - event: point
    when: 16.4.1072 TSC
    culture: tar
    title: '[Jems]() were established as the primary currency of [Tarsan]().'
  - event: point
    when: 22.6.1605 TSC
    culture: tar
    title: Jems were reworked as paper currencies with more reasonable quantities.
---

Jems are the primary currency of [Tarsan](). Because Tarsan has a tendency to be the baseline for most currencies, jems are used as the baseline currency which is used to compare against all other currencies.

# Initial Establishment

Jems were officially established as the currency of the country on `date: 16.4.1072 TSC; tar`. The original form were imprinted metal disks in 1j, 7j, and 49j quantities. The disks were gradually larger with the 1j being an inch across, the 7j at 1.5 inches, and the 49j at two inches.

Fractional jems, called chips ("c") were imprinted metal bars that were 1/49th of a jem.

# Modern Currency

On `date: 22.6.1605 TSC; tar` with an almost unanimous decision by the great families, Tarsan reissued jems in a new format, a purely paper format with various magical components and quantities that were easier to work with exchange.

The bills are all the same size at five inches by two inches with a brightly colored illustration. The material is sturdy with metal fibers woven into a dense paper.

The lowest seven denominations have a portrait of the current head of a great family on one side and a detailed illustration of the capital [family town](/tarsan/family-town/) for that family.

Quantity | Family       | Color
-------- | ------------ | -----
1j       | [Rinfir]()   | Red
5j       | [Joknig]()   | Orange
25j      | [Pun]()      | Yellow
50j      | [Kasin]()    | Green
100j     | [Wilim]()    | Blue
500j     | [Lamaster]() | Indigo
1000j    | [Disobin]()  | Violet

The chips were retained as physical bars but they were reissued at tenth jem (0.1j) pieces.

## Security Features

There are a number of security features on the bills with more extensive ones for higher denominations.

- [Feedback-sensitive](/feedback/) imprinting. When reacting to a strong magical feedback, dark marks appear in a certain pattern over the bill.
- Detectable [Resonance]() can be used with a commonly sold (and cheap) detection device. If the bill experiences too much feedback, this feature no longer becomes useful but it is visibly obvious when it has been triggered.
- Identification numbers. There are 2-5 of these numbers with the first one being in a widely distributed book called the [Book of Currency Verification]() and the others with more limited distribution. These numbers are used to verify the bill for reissue in case of magical damage.
- Micro-detail illustrations which use magic to create images too small for modern printing.

One of the primary complaints about the currency is the fragility of the resonance-sensitive features. Outside of the cities, they are frequently triggered which makes those features useless.

At the moment, these are considered acceptable because reissues due to the triggered features lets the great families (in specific, Rinfir) control the amount of bills in circulation and therefore the value of the jem.
