---
categories:
  - Kyōti Clan
  - Chobìre Clan
title: Pabinkúe
---

Pabinkúe is a herd spirit on the side of the moon.

# Members

```plugin
type: insert-category
key: categories
value: Pabinkúe Clan Member
```
