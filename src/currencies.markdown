---
title: Currencies
categories:
  - Major Topic
---

Almost every country and culture has, at one point, established a currency to assist with the exchange goods and services.

```plugin
type: insert-category
template: list
key: categories
value: Currency
```
