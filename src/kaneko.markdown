---
title: Kanéko
aliases:
  - Kosobyo Kanéko
  - Kanéko Lurkuklan
categories:
  - Person
sources:
  - Flight of the Scions
---

Kanéko is the daughter of a baron and a foul-mouthed warrior.
