---
title: Volis
categories:
  - Language
summary: >
  The written language that represents the telepathic communication in [Vo]().
task:
  - Add descriptions from Flight of the Scions.
  - Give some examples.
---

Volis is the written language that represents the telepathic communication in [Vo](). It was never intended to be spoken, only to represent the intricate communications of telepathy, in specific the way of addressing distinct memories in the group mind.
