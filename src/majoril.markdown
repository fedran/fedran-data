---
title:  Majoril
aliases:
  -  Majoril Jilkoris
categories:
  - Person
---

Majoril Jilkoris (Maj) is a farmer's daughter who volunteered for the Kormar army around the [Kormar-Gepaul Marches War]().

# Magic

Majoril's talent originally was creating sticks of various length. During her training, she learned how to create a sharpened version which she can summon with a snap of her fingers that ended with her finger pointing in the direction of the spear. The spear launches from her palm with considerable force. She can only manifest a single spear at a time.
