---
title: Kiramíro
categories:
  - Person
  - Shimusògo Clan Member
  - Clan Member
events:
  - event: birth
    when: 1436/4/24 MTR 8::62
---

Kiramíro was a warrior of the [Shimusògo]() clan who spent most of her adult life protecting her own clan and the surrounding valleys as part of a mutual defense agreement with Shimusògo's neighbors. Because of her frequent visits to other clans, she maintained a home among each of the clans she helped protect.
