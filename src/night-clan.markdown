---
title: Night Clans
aliases:
  - Moon Clan
  - Moon Clans
  - Night Clan
---

The phrase "night clan" or "moon clan" identifies a clan that gains its power from [Chobìre](), the moon spirit.

# Clans

```plugin
type: insert-category
key: categories
value: Chobìre Clan
```
