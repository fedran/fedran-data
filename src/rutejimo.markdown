---
title: Rutejìmo
aliases:
  - Shimusogo Rutejìmo
categories:
  - Person
  - Shimusògo Clan Member
  - Clan Member
  - Sand and Blood Character
  - Sand and Blood Primary Character
  - Sand and Ash Character
  - Sand and Ash Primary Character
  - Sand and Bone Character
  - Sand and Bone Primary Character
sources:
  - Sand and Blood
  - Sand and Ash
  - Sand and Bone
summary: "The slowest and weakest of the [Shimusògo]() clan."
characterPov: "0100"
events:
  - event: birth
    when: 1454/7/41 MTR 17::61
    secondary:
      - Chyojímo (Mother)
      - Hikòru (Father)
    referenced:
      - Desòchu (Brother)
  - event: point
    title: "[Rutejìmo]() accepted into the [Shimusògo]() clan"
    when: 1471/4/4 MTR 11::8
  - event: point
    title: "[Rutejìmo]() declared a [banyosiōu]() from the     [Shimusògo]() clan by [Desòchu]()"
    when: 1481/6/21 MTR 12::17
  - event: point
    when: 1482/8/13 MTR 14::40
    title: "Marriage ceremony with [Mapábyo]()"
  - event: death
    when: 1489/1/14 MTR 16::79
---

Rutejìmo (Jìmo for short) is a teenager of the [Shimusògo]() clan. While growing up, he was unremarkable and had little skill to make himself notable. But, over the years, he slowly became something more.

# Early Life

Rutejìmo was the youngest living son of Chyojímo and Hikòru. Born after three miscarriages, he was younger than his eldest brother, Desòchu, by fourteen years.

A week after he was born, his mother fell when she tripped on a path inside [Shimusogo Valley](). Already weak from Rutejìmo's birth, she died from blood loss and her injuries.

Four years later, Hikòru and Desòchu still blamed Rutejìmo for his mother's death. Hikòru drank heavily and became a disruption on the clan while Desòchu mimicked his father's actions and abused Rutejìmo in private.

```plugin
type: spoiler
source: Raging Alone
```

When it became obvious that his father was a danger to Rutejìmo, the clan kicked Hikòru out of the clan and declared him a [banyosiōu]()). Desòchu was left to care for his brother, but it was only days later when Desòchu almost shoved Rutejìmo off a cliff. Realizing that Desòchu was also unable to see past his rage, the clan forced Rutejìmo's older brother into his rite of passage. These events are told in [Raging Alone](), a serial that appeared in various issues of the [Journals of Fedran]().

# Growing Up

After the events in *Raging Alone*, Desòchu moved out of the family cave and Rutejìmo moved into his grandparent's cave. Living with [Tejíko]() and [Somiryòki]() was a different experience for Rutejìmo. He was no longer subject to the abuse from his father and brother, but Tejíko was strict and had a short temper. She beat Rutejìmo, but it was for not obeying her rules. The rest of the clan watched her carefully, to see if the abuse remained with the family, but Tejíko was well-known for her punishments and she doled them without bias to anyone who transgressed the rules.

Rutejìmo did not flourish with Tejíko. Over the next thirteen years, he struggled to keep up with the frequent trials and requests the elders inflicted on the clan's youth. It quickly became apparent that he was slower and weaker than anyone besides [Pidòhu](). The other children his age were all more capable, but not even them could keep up with the pride of the clan, [Chimípu]().

Frequently, Rutejìmo came to blows with [Tsubàyo]() and [Karawàbi](), but rarely won the resulting brawls since the two teenagers remained near each other and easily defeated Rutejìmo. Unable to prove himself to anyone, he found himself drifting without any aspirations to reach his brother's powers or Chimípu's obvious skill.

# Coming of Age

At the beginning of [Sand and Blood](), Rutejìmo was seventeen years old. He had not gone through his rites of passage because the clan elders felt that he did not have the emotional maturity to become an adult.

Being that *Sand and Blood* is written from Rutejìmo's point of view, there are relatively few descriptions of himself.

> [...] Like all desert folk, she had dark skin and green eyes. But where she was as dark as obsidian rock, Rutejìmo was the softer brown of sun-bright soil. She wore [...].
>
> In contrast, Rutejìmo wore a pair of white cotton trousers and remained bare-chested. A few sparse black hairs dusted his pectorals. The only representative traits of the Shimusògo were hard, muscular legs and lean bodies adapted to running across the desert for hours.
>
> --- [Sand and Blood 3](/sand-and-blood/chapter-03/)

Rutejìmo had dreams of keeping up with Chimípu even though he knew he wasn't capable of doing so. He chafed underneath her obvious skill, wishing he would push himself harder but knowing that he wouldn't.

```plugin
type: spoiler
source: Sand and Blood
```

Later, during the rite and when Chimípu ran off to try to find the elders, Rutejìmo was willing to let Tsubàyo and Karawàbi take charge. Even when Karawàbi was trying to knock Pidòhu off a rock, Rutejìmo felt guilt for not doing anything when the teenager fell and broke his legs. The same attitude remained with him when he let Tsubàyo bully him into abandoning Chimípu and Pidòhu and going off on their own.

It wasn't until Rutejìmo started to feel the power of Shimusògo and the abusive way that Tsubàyo treated him that he was willing to stand on his own. When Tsubàyo tried to steal horses, Rutejìmo finally stood up for himself and refused. That led to a fight with Tsubàyo and, later, Karawàbi, before Rutejìmo ran back to rejoin Chimípu and Pidòhu.

He realized his mistake and offered his throat to Chimípu who refused to take his life. She reluctantly allowed him to retain but didn't lash out.

Through the adventures that followed, Rutejìmo found that he had no taste for violence and was quickly becoming a pacifist. He made some attempts to fight Tsubàyo over Pidòhu, but he couldn't bring himself to attack the teenager. When he saw violence, he became sick to his stomach and would later beg to save Tsubàyo's life.

During this time, Rutejìmo also acquired a fear of the dark much to the realization that he was too weak to survive on his own. A night clan warrior, [Pabinkue Mikáryo](), enforced this when she saved him from a giant snake. It was during this time that he started to have feelings for the warrior that would later haunt him.

# Ten Years After

In the decade after the events of [Sand and Blood](), Rutejìmo didn't distinguish himself as anything other than a solid, though limited, courier for the clan. His speed never significantly improved and it was always held over him by giving him shorter runs and forcing him to do more jobs in isolation.

During this time, he had a number of near-death experiences, many he didn't tell the rest of the clan. The discomfort he had with violence near the end of *Sand and Blood* continued and he didn't enjoy or relish it; telling the clan would have them celebrating his prowess when he wanted to pretend it never happened.

> Rutejìmo's stomach twisted at the harsh threat. The last time he entered the city, he saw how violently the Wamifūko responded when the two men chasing him used magic within the walls. The sight of the eviscerated corpses burned themselves into his memories and nightmares.
>
> [..]
>
> Chimípu came up on the other side and clapped his shoulder. "So, you only had a little trouble with that delivery, huh?" She smiled cheerfully, although her fingers gripped the joint of his shoulder and sparks of pain ran up his neck.
>
> Rutejìmo blushed even hotter. "There... might have been a few problems."
>
> --- [Sand and Ash 8](/sand-and-ash/chapter-08/)

Only a few of the clan understood his struggles. His social interactions became more withdrawn until only [Hyonèku]() and [Gemènyo]() were considered friends. This is something they noted more than once:

> Rutejìmo could tell he was writing something obscene. With a grin, he slashed his hand through the smoke. "Old men like you shouldn't use words like that."
>
> "Old men like me and Hyonèku shouldn't have to invite young men like you over for cards."
>
> --- [Sand and Ash 1](/sand-and-ash/chapter-01/)

It was implied that Gemènyo wanted Rutejìmo to find a mate. In fact, they already had one in mind however Rutejìmo had developed a crush, also known as a [shikāfu](), for the memory of [Pabinkue Mikáryo] who he had met during the events of *Sand and Blood*. He had no other romantic interests and didn't take advantage of [hyoronibāga]() though he had been offered by most of the female warriors in the clan.

```plugin
type: spoiler
source: Sand and Ash
```

During this time, it became obvious to the clan that [Mapábyo]() had fallen for Rutejìmo, but he was so withdrawn that he wasn't aware of it. When she started to mimic his actions, including wearing an animal tooth necklace like he did, the elders of the clan realized that something had to change. They began to pressure him to get out of the cave, to interact with Mapábyo, and start guiding them toward a courtship.

Then a trip of [Wamifuko City]() and a chance encounter with Mikáryo threw everything into disarray. Rutejìmo dropped everything to spend a few nights with her but didn't tell anyone where he was going. They panicked and searched the city only to find him stumbling home from nights of lust and stories.

Enraged, [Desòchu]() and [Chimípu]() punished Rutejìmo for his actions but Desòchu wasn't satisfied. Enraged, he cast Rutejìmo from the clan for a year and declared him a [banyosiōu](), someone without a clan.

Beaten and injured, Rutejìmo returned to Mikáryo who also rejected him and wouldn't let he travel with her.

Alone, Rutejìmo retreated to a bar to drink away his money until Mapábyo showed up after breaking away to perform her mail run between Wamifuko City and [Monafuma Cliffs](). They had a brief fight that ended up with Mapábyo using her powers in the city and destroying a large number of store fronts with [feedback](). They were caught by [Wamifuko Gichyòbi]() but instead of enforcing the death penalty for felony feedback, he banned them from the city for the time it would take them to get to the cliffs and back.

Surprised and thankful, Mapábyo and Rutejìmo headed out along her mail run. Within a few days of travel, they had another fight and split apart.

When Rutejìmo arrived at the next oasis without her, he found out many of the unexpected problems of being a banyosiōu: no one would allow him shelter, give food or drink, or even talk to him. He was sent out into the desert without supplies.

He survived, but he was turned away from a number of oases until one took pity on him. He was given some instructions and expectations as a banyosiōu, both how he needed to remain silent and take chores that were offered to him. If he talked, he would be killed

Unable to do anything else, he listened and was given shelter and supplies to chase after Mapábyo. When he caught up with her, he apologized for his actions and they realized they loved each other.

When they returned to Wamifuko City after the mail run, Rutejìmo worked his way through the banyosiōu society to secure a cheap rental to surive and got a job for an underworld boss. The high points of his life was when Mapábyo would come back to the city and they would spend a few days being lovers before they had to split again.

That worked for a number of months until [Tejíko]() and others came to bring Mapábyo home along with Rutejìmo in tow though they would address him directly.

At home, things were not better for Rutejìmo. He had endless streams of chores but no one would talk to him except for Mapábyo. However, his friends and Mapábyo's parents did go through significant effort to make sure the couple had time together in private.

When Mapábyo had to go on an extended trip, Rutejìmo's struggles grew even worse. He felt suicidal and depressed. Unsure he could survive without her, he decided to head back to Wamifuko City, though the trip to the city could have been fatal for him. Only a last-minute effort by [Chimípu]() and [Pidòhu]() stopped him and showed him that he had friends even with Mapábyo gone.

When Mapábyo finally came back, their celebrations resulted in her being pregnant. He found a happy medium of working while being ignored but also took on the duties of taking care of the dead and diseased in the surrounding valleys. Later, he would find out he was becoming one of the [kojinōmi]() when [Jyuyanófu]() gave him the [Book of Ash]() and mute instructions on how to pray for the dead.

The only person unhappy with his arrangement was Desòchu who confronted Rutejìmo. To Rutejìmo's surprise, his friends and family defended him against his brother. However, that left a gulf between the two brothers that continued to grow worse with every passing day.

On one sunny day, as the clan enjoyed themselves, Rutejìmo was coming home from hauling garbage. Hyonèku and [Kiríshi]() slyly told him that Mapábyo was alone in the cave.  He was heading home when he saw [Nigímo]() about the fall from a cliff. He broke his silence to yell for help but no one could move fast enough. He ended up destroying the clan's mechanical dog, [Opōgyo](), to alert his brother.

Desòchu rushed to help after using [The Call](). From inside the valley, Mapábyo responded and rushed up to give Desòchu enough speed to shoot straight up to save the two-year-old child but in the process, she had a miscarriage and Gemènyo sacrificed his life to save his wife.

That night, Rutejìmo burned funeral pyres for his own child and his best friend. Desòchu came and apologized, confessing that he struggled with his brother's path in life that was so much different than the others.

The rest of Rutejìmo's banyosiōu went relatively peacefully. On the final day, he finished his duties taking care of the dead and re-entered society as a speaking member once again. His first words was to ask Mapábyo to marry him.

# Marriage and Children

Between [Sand and Ash]() and [Sand and Bone](), Rutejìmo had two children with [Mapábyo]().

# Final Days

```plugin
type: spoiler
source: Sand and Bone
```

Five years after the events in [Sand and Ash](), Rutejìmo would being the final days of his life in [Sand and Bone](). At this point, he had gotten comfortable with his roles as the slowest courier in the Shimusògo and also as a kojinōmi with a remarkably far-ranging reputation:

> It was a young man, in his early thirties, with pale skin and intense green eyes. They met Rutejìmo's. The man was a warrior, with a bow slung over his back and five throwing knives at his right side. On his left, a long blade danced in the light, reflecting the name of his clan: Demuchìbyo.
>
> The warrior jerked and his eyes widened. "No," he said in a soft, lilting voice. He looked up and past Rutejìmo's shoulder. "You did not say you wished to trap Great Shimusogo Rutejìmo."
>
> Rutejìmo inhaled sharply. He didn't know the man in front of him, but he knew of the clan. They were scouts who made no noise in darkness when they chose silence.
>
> --- [Sand and Bone 28](/sand-and-bone/chapter-28/)

He had gained respect from other kojinōmi also:

> "He's a sun from the bright flames so I figured there would be pissing. None of us really fight when being a kojinōmi, but you'd be amazed how much of an ass we can be while silent. But, to my surprise, he didn't walk past me but started to help. He didn't care who it was, only that it got to the right pyre. He even knew the prayers of Chobìre when he helped me. I saw his lips, he knew the words."
>
> She chuckled to herself, her shoulders shaking. "I have never seen one who truly gave himself to the desert. He just hauled bodies to the right places. Said the right things. Moon and sun, night and day."
>
> --- [Let His Memory Go 4](/let-his-memory-go/chapter-04/)

When a Shimusògo shipment is robbed and the clan was unable to recover the money, they decided to take the risk of a high-paying job across the desert in [Kosobyo City](). Despite resistance from some of the couriers chosen to go, Rutejìmo was asked because of his skills as a kojinōmi.

Just as they reach the city, Rutejìmo is called away for his kojinōmi duties which takes a while to perform. He approached but is surprised when a Kosobyo kojinōmi, [Dochiryùma](), has ignored the request because it was for a night clan. Rutejìmo continued on because it felt right.

When he approaches the scene of the death, a family of night clan are surrounded by day warriors. [Tsupòbi]() tries to block Rutejìmo from performing his duties, much to the amusement of others. Rutejìmo attempts again but when he is blocked a second time, he feels a sudden urge to speak not for Shimusògo but as a member of the [Mifúno](), though he was not aware anyone could speak for the desert. The words have power and he declares Tsupòbi as dead to the desert, effectively making him a banyosiōu with the authority of the desert spirit.

Stunned and terrified, everyone else parted and let him take care of the dying girl he was summoned for. Later, he would meet the girl's grandfather ([Fidochìma]()) who is looking for revenge. Rutejìmo tells him to let the desert handle it and they part ways.

In Kosobyo City, Rutejìmo meets up with [Dimóryo]() who appears to be a street guard. She helps him find the rest of the clan after asking a few questions. Later, she would show up while Rutejìmo was shopping with his wife; she would express interest that Rutejìmo didn't seem like the rest of the kojinōmi.

Later that day, [Nifùni]() goes missing and the rest of the clan looks for him. Rutejìmo finds Nifùni about to accept an offer from [Techyomása]() however Rutejìmo stops it because something feels wrong. Nifùni later went back to accept the job, but ended up seeing Techyomása's assassination and the clan had to flee Kosobyo City in the middle of a fight.

Later, when Desòchu and Chimípu are punishing Nifùni, Rutejìmo stops his brother from declaring Nifùni banyosiōu by formally declaring that he spoke for Mifúno, which causes him to pass out.

When he recovers, he finds out that Kosòbyo and his allies were looking for the Shimusògo runners. They open the scroll case that Nifùni had gotten from Techyomása and find out that Kosòbyo was planning on breaking ways with [Tachìra](), an act that had caused civil war the last time it had happened. Realizing that they couldn't go public with their knowledge with Kosòbyo being one of the most powerful clans, they decide to race home and bring it up to a vote with everyone. If they were wrong, the Kosòbyo would destroy Shimusògo. If they were right, they had a chance to save the desert from a massive war.

With Rutejìmo the slowest, Desòchu gave him a path that would take advantage of his kojinōmi talents and powers. Thought, when the runners went their separate ways, Desòchu revealed that he was going to stay behind and sacrifice his life to slow down their pursuers.

Devastated, Rutejìmo was forced to run alone and face his fear of dark. He would later meet up with Nifùni for a short period before Rutejìmo was called away for his kojinōmi duties which he wasn't willing to forgo even while running for his life. He would later find out Nifùni was killed while he was tending the dead.

By the time he made it to Wamifuko City, he was surprised to find he was the only one who had survived. However the Kosòbyo and allies were already there. Gichyòbi defended Rutejìmo and declared war on the Kosòbyo in the process, breaking a century-old truce between the two clans.

As the Kosòbyo started to besiege the city, Rutejìmo found out that the Kosòbyo had attacked Hyonèku and Kiríshi, killing her in the process. Upset, Rutejìmo speaks for Mifúno and formally declares war on the Kosòbyo.

Rutejìmo would escape Wamifuko City to burn Kiríshi's body before racing home.

On his way to the next step planned by his brother, he sees the smoke indicating a kojinōmi was needed. Despite being close to home, he approaches it to find out it was a trap set by the Kosòbyo. He ends up making a Call which summons various warriors of the surrounding clans but it was [Chimípu]() who finally arrived to save him from being killed. She dies in the fight and he is forced to keep going.

His next step was [Three Falls Teeth]() where he met up with Dimóryo wearing Mapábyo's skin. In the fight that follows, Rutejìmo is fatally poisoned and Dimóryo cursed to never touch the desert again.

Wounded, Rutejìmo races home but doesn't make it. Just as he is about to pass out, Mikáryo finds him and stalls the poison for a night. Between her and [Tsubàyo](), they keep him alive until morning. However, the distance left to travel is more than he had ever ran but he knew that he wouldn't survive another night. He pushes himself harder than he ever had, sacrificing everything to make it and deliver the message.

When he does make it home, he succumbs to exhaustion and poison. In his dying breath, [Piróma]() take on the mantle of a kojinōmi and he spends his last moments giving her all the instructions he could.

[Sand and Bone]() ends with Rutejìmo's death.
