---
title: Miwāfu
categories:
  - Kyōti Topic
  - Language
---

Miwāfu is the language of [Kyōti](), the desert world. Like the many varied clans across the desert, Miwāfu is filled with inconsistencies and dialects. However, there is one "official" version of the language which was gathering by a group of scholars in both [Tarsan]() and Kyōti and is slowly gaining acceptance in general.
