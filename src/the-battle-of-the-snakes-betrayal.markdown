---
title: The Battle of the Snake's Betrayal
categories:
  - Wars
  - Kyōti Topic
  - Mechanical War Topic
events:
  - event: point
    when: 1489/3/44 MTR 19::77
    culture: kyo
    title: The first blow of Snake's Betrayal is made
    name: start
---

The Battle of the Snake's Betrayal is considered the first aggression of the [Mechanical War](), Fedran's first and only world war. It was also the beginning of the end of the [Crystal Age]() and the beginning of the [Mechanical Age]().

```plugin
type: spoiler
source: [Sand and Bone, Desert Child]
```

It set the forces of the moon clans up against the amassed armies of the sun clans as [Kosòbyo](), one of the first and strongest allies of [Tachìra](), prepared to break his allegiance to the sun spirit in an attempt to ascend a position equal to the great triad.

Most of the allies of the snake spirit came from the moon spirit, [Chobìre's](), who was given the impression that the snake intended to change allegiance over to [Hizogoma Betrayal](). They gathered to defend their new ally which would have been a major blow against the forces of the sun.

However, the Kosòbyo clan had no intent of joining the moon clans. Instead, they were planning on using the nearly limitless energies of a dragon spirit currently possessed by [Ruben]() to grant the snake spirit enough power to stand on his own and became a peer to the three great spirits of the desert: Tachìra, Chobìre, and [Mifúno](). If successful, the repercussions to the desert society would have been devastating, but historians couldn't agree that the result civil war that followed was better or worse than what Kosòbyo's ascension would have caused.
