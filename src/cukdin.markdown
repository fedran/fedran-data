---
title: Cukdin
aliases: [Cukdins]
summary: >
  The primary currency of [Gepaul]() with a standard abbreviation of "cuk".
categories:
  - Currency
  - Gepaul Topic
---

Cukdins are the primary currency of [Gepaul](). It was established during the split between [Tarsan]() and Gepaul. Unwilling to continue using [jems]() because of their association with Tarsan, the founders of Gepaul established a new currency based on the only item of worth they were able to bring with them: their wedding bracelets.

Cukdins comes from two Lorban words: ring ("cukla" or "cuk") and currency ("jdini", "din"). The abbreviation is "cuk".

# Physical Coinage

Since the bracelets were too large to be used as functional currency, the actual coinage is smaller and sized more for a ring. This is also why Gepaul uses [wedding rings]() instead of [wedding bracelets]().

The coinage for a single cukdin is a ring one inch across with the band being three-tenths of an inch thick. The inside of the ring has a single notch that is one-tenth of an inch deep and wide. The outside has an inscription written that traverses the entire circumference. The top surface of the ring has six fine concentric rings while the bottom has reeding (short ridges going from the outer to the inner rings). The metal is an alchemical alloy that gives the metal a orange sheen.

There are other denominations of the cukdin coinage:

Name       | Abbrev | Value   | Color  | Diameter | Notches
========== | ====== | ======= | ====== | =======: | ======:
pipacukdin | pip    | 0.1 cuk | red    | 0.8 in   | 0
cukdin     | cuk    | 1 cuk   | orange | 1.0 in   | 1
mucukdin   | muc    | 5 cuk   | yellow | 1.2 in   | 2
remucukdin | rem    | 25 cuk  | green  | 1.4 in   | 3

In each case, the inscription is a different quote from a poem that is changed every ten years.

# Paper Currency

Gepaul was one of the first countries to switch to paper currency. It has much of the same attributes as the individuals including colors and the inscriptions but there were differences to handle the new format.

Unlike the different sizes of coins, each bill is 2.5 inches by 3.5 inches and made of a thick, sturdy stock. Instead of notices, the cards has a number of holes punched along one side.

Name         | Abbrev | Value   | Color  | Notches | Side
============ | ====== | ======= | ====== | ======: | ====
cukdin       | cuk    | 1 cuk   | orange | 1       | Right
mucukdin     | muc    | 5 cuk   | yellow | 2       | Bottom
pano         | pan    | 10 cuk  | green  | 3       | Left
muno         | mun    | 50 cuk  | blue   | 4       | Top
panono       | panon  | 100 cuk | indigo | 5       | Right
munono       | munon  | 500 cuk | violet | 6       | Left
