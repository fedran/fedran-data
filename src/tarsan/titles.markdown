---
title: Titles
categories:
  - Tarsan Topic
---

Titles are as important to [Tarsan]() culture as families. As such, the nature of relationships is encoded into the title and used to address individuals. In [High Society](/tarsan/high-society/), titles are used extensively however in other classes, the use is diminished to only more formal situations.

# Gender

Gender is one of the most important component of someone's title. The base title is based on male or female:

- "sire" indicates that the title applies to a man.
- "dame" indicates that the title is for a woman.

## Transgender Titles

Before the 1820s, society struggled with the concept of transgendered individuals. In those cases, the title was usually gendered toward the physical sex of the individual. However, in the 1840s, there began a trend of transgendered individuals leaving their home town to "reset" their gender markers. This could any of the following:

- Moving to a country estate or home for a number of years before returning.
- Moving to another [family town](/tarsan/family-town/) with the help of family members.

After that, the title would address the correct gender of the individual.

Like many places, calling attention to the differences between sex and the gender of the title could be embarrassing or require more drastic efforts to maintain the correct gender identifier. This wouldn't relax until well into the 2000s.

# Relationship Qualifiers

Until the 1900s, the use of just "sire" or "dame" was not in favor. In almost all situations, a relationship qualifiers was added to the beginning to indicate where the individual stood when it came to family.

- "mo-" referenced children under the age of consent and individuals who were not able to legally speak for themselves.
  - Modame Emiris dea Kasin
- "be-" indicated an individual who was of the age of consent (sixteen years) and was unmarried.
  - "-ma-" could be used for divorcees looking for a second or later marriage. This use was almost always insulting since divorcess was highly discouraged until the late 1900s.
- "ta-" was used for married and useful members of society.
  - "-la-" was rarely used for widowers and widows when it became important to qualify that nature. This would be dropped when the individual remarried.
    - Taladame Penir da Kasin
- "ku-" was a term for unmarriable men or women. It equated to spinster, bum, or other insulting terms. It's use was almost exclusively used to describe unmarried women over the age of twenty-five or women who were not pretty to the cultural standards (e.g., overweight, non-blonde hair). However, mose uses were discouraged in public except for older unmarried women.
  - Kudame Lily kia Kasin

## Couples or Groups

When referring to multiple members of similar titles, there were a few abbreviations that developed in the 1600s that remained in use for centuries later:

- "rosire" was used to reference all men present.
  - The relationship qualifier could also be used such as:
    - Tarosire dea Kasin (All married men of Kasin)
- "rodame" could be used to reference all women being addressed:
  - Rodame
  - Rodame dea Kasin
- "-sor" indicated members of both gender. This is frequently used with another qualifier:
  - Tasor (a married couple)
  - Tasor dea Kasin (a married couple of the Kasin family)
  - Mosor (children of both genders)
