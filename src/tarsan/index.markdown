---
title: Tarsan
categories:
  - Country
events:
  - type: event
    event: point
    when: 0.0.436 TSC
    culture: tar
    title: Jolia invasion of the Countless Hills
  - type: event
    event: point
    when: 19.6.436 TSC
    culture: tar
    title: The first Council of Tarsan
  - type: event
    event: point
    when: 41.0.437 TSC
    culture: tar
    title: The Battle of Magdaleon
  - type: event
    event: point
    when: 27.4.438 TSC
    culture: tar
    title: The Battle of Polenus
  - type: event
    event: point
    when: 46.4.438 TSC
    culture: tar
    title: Establishment of Tarsan
  - type: event
    event: point
    when: 35.6.440 TSC
    culture: tar
    title: Tarsan controls the Countless Hills
  - type: event
    event: point
    when: 0.0.441 TSC
    culture: tar
    title: The first regular Council of Tarsan
---

Tarsan is a [country]() located in the north-western coastal region of the continent. It borders directly against [Gepaul]().

Despite being a relatively small country, Tarsan has provided significant influence over civilization in the northern and western parts of the [continent](). Much of this comes from Tarsan's history as the oldest, established country on the continent.

# History

## Countless Hills

Before Tarsan was established, much of the region was known as the [Countless Hills](). The hills were claimed by various feuding families, clans, and tribes. Alliances were formed and broken fluidly and it was a harsh The alliances and opposition were fluid during this time with betrayal common as were short-lived treaties.

When [Jolia]() invaded in 436 TSC, there was almost no organized resistance or armies capable of working together enough to defeat the invaders. The southern-most families quickly fell, either by subjugation or destruction, and it quickly looked like the entire Hills region would be conquered within a few years.

It wasn't until a wintry morning 19.0.437 TSC that eight families along the northern coast gathered together in what would be the first [Council of Tarsan](). The [Kasin]() family was the one who made deals and treaties to make this possible since most of these families had been fighting only recently with each other. Together, these families decided to set aside their differences to fight off the invaders. Much of the following days were spent in planning to take on the initial wave of invaders.

```plugin
type: insert-category
template: list
key: categories
value: Tarsan Founding Family
```

However, what the [Galadir]() family didn't know was that the other seven families were setting them up to be sacrificed as a decoy. During the first major battle, the [Battle of Magdaleon](), the betrayal was complete and the Galadir family was completely destroyed to give the other seven families a chance to flank and destroy the battalion of invaders.

Encouraged by their success, the newly formed [Army of Tarsan](), as it would be known, cut through the supply lines of two other Jolia battalions and began to a guerrilla attack against the invaders. These moving battles would last for close to eleven months years until the [Battle of Polenus]() when Tarsan was able to completely cut off the Jolia invaders and collapse the [Polenus Pass](), the primary route from Jolia into the Countless Hills.

In the first battle with the invaders, the other seven families sacrificed the [Galadir]() family in a battle that caused the complete destruction of the Galadirs and allowed the other armies to flank the invaders and defeat the first wave of invaders.

## Early Years

After the army finished defeating the invaders, the seven families sat down at the Council of Tarsan to formally break apart. However, five of the families (excluding [Pun]() and [Joknig]()) decided that being banded together had given them almost complete control over the Countless Hills. The remaining two families were given an ultimatum, either permanently join into the newly formed country of Tarsan or be destroyed. They both formally accepted.

Using their military might, Tarsan turned their attention to the other surviving families, tribes, and clans. Even with superior force, it took them until 440 TSC until they had control over the region.

## Council of Tarsan

Starting 0.0.441 TSC, it became a tradition that the Council of Tarsan would meet first week of the year. They would rotate the founding family that would host the council. After the somber affair of the council, the host family would then arrange celebrations throughout the city to honor Tarsan. These celebrations, over the next thousand years, would become formalized into what is now known as the Tarsan [Social Season](/tarsan/social-season/).

# Families

Tarsan politics are organized by families, with the seven great families at the top of society and various branches and lesser families branching out through various relationships of blood and marriage.

Most families are patriarchal, with the eldest male becoming the head of the family. Only when there are no living males in the eldest generation will a female become head of the family. In the seven great families, they will skip a generation to avoid a female head.

In addition to the founding families, there are also a slew of lesser families that were either joined, subjugated, or created over the years that would follow.

```plugin
type: insert-category
template: list
key: categories
value: Tarsan Lesser Family
```

# Culture

Tarsan culture is ritualistic from the highest ranking society members to the lowest indentured servant. The way of addressing each other, the times of day to perform various actions, and even the social calendar are all dictated by the patriarchs of the seven great families, as it has been for centuries.

# Additional Topics

```plugin
type: insert-category
template: list
key: categories
value: Tarsan Topic
```
