---
title: Social Classes
categories:
  - Tarsan Topic
---

As an older patriarchal and familial society, Tarsan is stratified into a multitude of classes bound by tradition and rituals.

```plugin
type: insert-category
template: list
key: categories
value: Tarsan Social Class
```
