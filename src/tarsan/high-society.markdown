---
title: High Society
categories:
  - Tarsan Topic
  - Tarsan Social Class
summary: >
  High Society are the upper strata of [Tarsan](/tarsan/) society.
---

One of the most famous and visible classes in Tarsan is High Society. This is the uppermost strata of society where the rich and powerful spend their days meddling with politics, interacting with [families](/tarsan/families/), making and losing millions of [jems](), and attending various grand events that showcase wealth and prosperity.

Even though High Society reflects less than 1% of the population in the country, this is the view that most people outside of Tarsan see. Because of this, many other countries and cultures track various events of High Society along with trying to alternately duplicate or demonize the visible signs of wealth and tradition.

# Gentlemen

As with most parts of Tarsan society, men dominate the visible stage. They have two parts: to be wealthy and to demonstrate that wealth. It isn't enough to own nine-tenths of all businesses and lands across the country, it has to be obvious they have that wealth. This means hosting expensive games, grand balls, and hour-long meals. They publicly trade or wager their businesses and lands all while trying to get their names plastered on everything they can.

# Ladies

For women, their roles are much like a business or property, something to be purchased and then presented as an accessory to a man's wealth.

> Without a beautiful wife, there is a limit of how prosperous a man truly can be.

High Society ladies usually have their own wealth, in some cases it can be more than their husbands. Normally, a husband "owns" his wife's property as much as he "owns" her, but there is also a distinction between the two. While a lady can own any type of businesses, they typically focus their investments on the "silk businesses" or ones that focus on the female-dominated industries such as textiles, event planning, and artistic industries.

# Leaving Society

There are consequences of losing too much wealth. Too many poor deals or lost wagers could result in balances a little too close for gossip to permit. In those cases, a gentleman may gracefully bow out of High Society by moving to a countryside manor or embark on a trip and not returning. Sometimes, they bring their wives along and other times they completely abandon all semblances of their lives and leave to reinvent themselves. Ungracefully, they can choose to remain in their family cities where they shift to a lower class. Over time, their names stop circulating in the gossip circles and they cease to exist as far as High Society is concerned.

There are other ways to leave High Society. One of the more common is to have a job. There is a fine line between owning a business and actually working for a business, even as an owner.

For ladies, it is easier. They follow their husband's fate. If he remains High Society, it is difficult for them to be cast out. Likewise, it doesn't matter how powerful they are if their husband falls.

# Entering Society

While leaving High Society is relatively easy, joining it is not but the methods are different for men and women.

## Gentlemen

There is an obvious requirement of having wealth. The specific amounts are less important other than it needs to be excessive and obvious. This can be almost any industry or even a mixture of industry. For young men, their families may provide the initial seed money by granting ownership or a percentage of family-owned businesses. This allows a family to ensure that any promising male is able to maintain their High Society lifestyle.

There is also a more social component to entering High Society: the boon. The boon can be any form, but it represents a social acceptance into High Society. This boon can granted as a decision by a family's patriarch, exemplary military service, or as a reward for unique skills.

## Ladies

Women entering High Society have a much different route. One is to be married to a man who has acquired enough wealth and granted a boon of entrance. The other is to marry such a man.

The marriage process is a complex affair that involved presenting debutantes (bedames) to eligible gentlemen (besires) through a series of balls, feasts, and public events that are held during the so-called [Social Season](). The debutantes start their first Social Season somewhere between the ages of sixteen and eighteen. Most bedames can be identified by outfits that always consistent of cream-colored fabric, a color seen as "pure" or "innocent".

Over the centuries, the Social Season has become a highly ritualized process where everything is coordinated together according to the most common fashions. Frequently, these changes on a weekly basis so each week has a different theme, sport, event, or even shoe style. The intent is to present the debutantes in the most admirable manner.

Over the various events, besires will express interest for the hand of a debutante to the young lady's father. Treating their daughter's hand as an investment, a father can demand higher costs for her hand or wait for other bids. It is not uncommon for a bedame to have no knowledge or awareness of a besire is bidding. They are also not typically present when the negotiations are completed, the first time a bedame becomes aware that they have been betrothed is during an announcement.

At the end of the season, daughters who are not betrothed have a few months before the next Social Season where they will once again try to attract a husband but their desirability is diminished by the passing year and competition renews with the next round of women.

Any bedame that remained un-betrothed after ten years is considered a spinster and becomes a kudame. Kudames are forced out of High Society in a matter of days or months and are quickly forgotten.

Besires, on the other hand, typically attend Social Event balls starting at eighteen to twenty years of age up until their forties or fifties. Society typically considered a five year difference in age between husband and wife to be "ideal" with the husband always older than the wife.
