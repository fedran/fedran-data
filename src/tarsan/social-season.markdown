---
title: Social Season
categories:
  - Tarsan Topic
---

The "Social Season" is a period of time between the vernal and autumnal equinoxes. This corresponds to 45.0 to 32.4 of the [Tarsan Standard Calendar]() (TSC). The actual start of the season is midnight of the first (nodei) on or after the vernal equinox and goes until midnight of the last seventh (xadei) day on or before the autumnal equinox.

The Social Season is mostly assumed to centered around the [family town](/tarsan/family-town) because most rural manors aren't close enough to the events to participate. Those who want to avoid the politics and affairs of the Social Season frequently leave for the rural manors during this time.

After centuries of tradition, the Social Season is highly regimented to ensure that not only each year has its own relative theme but each week (starting on nodei) also has a distinct color theme and style. These fashions are typically decided by the men of the family and frequently reflect the slowly change traditions of the patriarchal view of the season. The themes for the season are announced across the country at the start of spring (0.0 TSC) and usually get into the hands within a number of weeks.

This constantly changing traditions also ensures steady (and occasionally frantic) business for milliners, seamstresses, and almost the entire rest of the [silk industries](/silk-industry/). It also means that almost no part of an outfit can carry from year to year which creates a constantly changing tapestry but the restrictive manner the themes are presented don't allow for much deviation from a slowly moving definition of "beauty" within the country.
