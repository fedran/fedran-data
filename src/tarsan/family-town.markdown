---
title: Family Town
categories:
  - Tarsan Topic
---

To handle the issues of [feedback](), cities and even villages of [Tarsan]() were owned by a family which dictated the [resonance]() of all magic within the town.

Like other [naming conventions](/tarsan/naming-conventions/), towns would use "tca" in place of a father's or mother's qualifier.

- Soldir
- Soldir tca Kasin
