---
title: Naming Conventions
categories:
  - Tarsan Topic
---

Like most cultures, a name of a character or place is very important. Tarsan names have a tendency to reflect the family that owns or controls what is being named, but they still focus on the individuals.

# Characters

A person's name starts with their given name and a number of clarifications, the family name being the most common to be seen. Like most [Lorban-based](/lorban/) names, Tarsan names begin and end with consonants. *If a name does not end in a consonant in narrative, it usually means it translated to fit English. For example, [Lily]() is written and spoken as a different name.*

For example:

- Kendrick
- Glorias ("Gloria" in English)

The given name is the most commonly used name unless someone is being formal.

## Primary Family Qualifier

A couple is married under the auspices of a single family. This is the binding (lazlaha) for their relationship and typically is the family of one or both sides of the couple.

- "de" names of the husband's family such as:
  - Falim de Kasin
- "da" names the wife's family such as:
  - Sarlin da Kasin
- "dea" is used when the husband and wife comes from the same family such as:
  - Kendrick dea Kasin

## Secondary Family Qualifiers

The secondary or lesser family uses a similar pattern, except that the qualifiers are slightly different:

- "hi" names the husband's family, such as:
  - Hasan hi Pilnok
- "ho" is used for the wife's family:
  - Falim ho Maifir

There is no shared family with lesser families, in that case, they would have only a primary family which would use the "dea" qualifier.

Multiple qualifiers can be used in this case, usually the primary family is listed first. The reverse is done, but that is almost always considered an insult since it emphasizes a lesser family.

- Falim de Kasin ho Maifir
- Sarlin da Kasin hi Genifir

## Small Families

Some families are quite large and there is bound to be confusion with similar or identical names. If additional clarity is required, then the parent's name can be given after the appropriate qualifier.

- "tor" indicates the father's given name, such as:
  - Emiris dea Kasin tor Hasan
- "tal" indicates the mother's given name, such as:
  - Lily kia Kasin tal Sarlin

In most cases, only the father's name is used to clarify identity however there are situations when both the mother and the father need to be identified:

  - Emiris dea Kasin tor Hasan tal Mindil

If a parent of a parent relationship is needed, then the qualifiers are just combined with the parental qualifier. This is also used when the father is not important.

- Emiris dea Kasin tor Hasan tortor Gimdal
- Emiris dea Kasin tortor Gimdal
- Emiris dea Kasin tor Hasan tortal Kilis

## Changing Families

Families can also "adopt" or acquire individuals. This is rarer and generally considered both a form of respect (they were good enough to join another family) and disrespectful (they can never understand what it was like to grow up in that family). These also a qualifier:

- "bo" is used to indicate an adoptive family. This is always used in place of the primary but the secondary family can indicate the former primary family. For example, when Kendrick dea Kasin left the Kasin family for the Martin family, he became Kendrik bo Martin ha Kasin

## Children

For children and unmarried individuals, the name is always the name of the parent's primary family and indicated with "kia" such as:

- Lily kia Kasin
- Galadin kia Kasin

Children of adopted parents are considered full members of the new family so there is no "bo" form of their names.
