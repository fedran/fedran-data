---
title: Mandate of Ear and Tail
events:
  - event: point
    when: 51.6.1837 TSC
    culture: tar
    title: Mandate of Ear and Tail
  - event: point
    when: 1841 TSC
    culture: tar
    title: Most of the known world is aware of the Mandate of Ear and Tail
---
