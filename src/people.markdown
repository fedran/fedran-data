---
title: People
categories:
  - Major Topic
  - Site Navigation
---

There are a lot of people in the world of [Fedran](/), many of them are only referenced by name or in a quote.

```plugin
type: insert-category
key: categories
value: Person
```
